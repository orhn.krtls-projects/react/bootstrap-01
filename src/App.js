import React, { Component } from 'react';
import Footer from './components/Footer';
import Header from './components/Header';
import AppContextProvider from './contexts/AppContext';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Solutions from './components/Solutions';
import Contact from './components/Contact';
import NotFound from './components/NotFound';
import JsReactSolutions from './components/JsReactSolutions';

class App extends Component {

  render() {
    return (

      <AppContextProvider>
        <div className='container-fluid'>
          <Header />
          <BrowserRouter>
            <Routes>
              <Route path="/" exact element={<Solutions />} />
              <Route path="/solutions" element={<Solutions />} />
              <Route path="/contact" element={<Contact />} />
              <Route path="/js-react-solutions" element={<JsReactSolutions/>} />
              <Route path="/*" element={<NotFound />} />
            </Routes>
          </BrowserRouter>
          <Footer />
        </div>
      </AppContextProvider>

    );
  }
}

export default App;
