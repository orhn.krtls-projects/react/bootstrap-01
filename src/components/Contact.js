import React, { Component } from 'react'

export default class Contact extends Component {

    changeInput = (e) => {
        this.setState(
            {
                [e.target.name]: e.target.value
            }
        )
    }

    sendMail = (e) => {
        e.preventDefault();
        const { email, message } = this.state;
        const newEmail = {
            email: email,
            message: message
        }
        console.log(newEmail);
    }
    render() {
        return (
            <div className="container mt-5 px-5">
                <form onSubmit={this.sendMail}>
                    <div>
                        <label htmlFor="exampleInputEmail1" className="form-label">Email address</label>
                        <input name='email' onChange={this.changeInput} type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                    </div>
                    <div className="mb-3">
                        <label htmlFor='' className="form-label">Message</label>
                        <textarea name='message' onChange={this.changeInput} className="form-control" aria-label="With textarea"></textarea>
                    </div>
                    <button type="submit" className="btn btn-primary">Send</button>
                </form>
            </div >
        )
    }
}
