import React from 'react'

function Footer() {
    return (

        <div className="container-fluid mx-5 text-md-center">
            <div className='row-md-12'>

                <div className='col-md-12'>
                    <a href='terms-of-use' className='text-secondary'>Terms of Use</a>
                    <span className='text-muted mx-2'>| </span>
                    <a href='privacy-policy' className='text-secondary'>Privacy Policy</a>
                </div>

            </div>
            <div className='row-md-12 text-md-center text-secondary'>
                <p className='fs-6'>Copyright @ 2021 e-Solutions</p>
            </div>
        </div>

    )
}
export default Footer;
