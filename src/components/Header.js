import React, { Component } from 'react'
import { AppContext } from '../contexts/AppContext';

class Header extends Component {

    static contextType = AppContext

    constructor(props) {
        super(props)
    }

    onClickEvent(value) {
        let lang = String(value)
        this.context.setLanguage(lang)
        localStorage.setItem("language", lang)
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container-fluid">

                    <span className="navbar-brand">
                        <img src="logo.png" alt="" width="30" height="30" className="d-inline-block align-text-center" />
                        Havelsan
                    </span>

                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <a href='/' className='nav-item nav-link'>Home</a>
                            <a href='solutions' className='nav-item nav-link'>Solutions</a>
                            <a href='about' className='nav-item nav-link'>About</a>
                            <a href='contact' className='nav-item nav-link'>Contact</a>
                        </ul>
                    </div>

                    <div className="dropdown">
                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownLsnguage" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        {localStorage.getItem("language")}
                        </a>
                        <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a className="dropdown-item" href="#" id="EN" onClick={() => this.onClickEvent('EN')}>EN</a></li>
                            <li><a className="dropdown-item" href="#" id="SW" onClick={() => this.onClickEvent('SW')}>SW</a></li>
                        </ul>
                    </div>                   
                </div>
            </nav>
        )
    }
}

export default Header;
