import React, { Component } from 'react'

class JsReactSolutions extends Component {

    render() {
        return (
            <div className="container-fluid">
                <div className="row row-cols-2 mb-5">
                    <div className='coll-md-4'>


                        <div id="carouselExampleIndicators" class="carousel carousel-dark slide" data-bs-ride="carousel">
                            <div className="carousel-indicators">
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                            </div>

                            <div className="carousel-inner">
                                <div className="carousel-item active">
                                    <img src="js-react-01.png" alt="Los Angeles" className="d-block w-100 rounded" />
                                </div>

                                <div className="carousel-item">
                                    <img src="js-react-02.png" alt="Chicago" className="d-block w-100 rounded" />
                                </div>

                                <div className="carousel-item">
                                    <img src="js-react-03.png" alt="New york" className="d-block w-100 rounded" />
                                </div>
                            </div>

                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>

                    </div>
                    <div className='coll-md-8'>
                        <div className="card-body">
                            <h5 className="card-title">Demo - 1</h5>
                            <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="moreinfo-js-react" className="btn btn-primary">Run Demo</a>
                        </div>
                    </div>

                </div>
                <div className="row row-cols-2">
                    <div className='coll coll-sm-8'>
                        <div id="carouselExampleIndicators2" class="carousel slide" data-bs-ride="carousel">
                            <div className="carousel-indicators">
                                <button type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                                <button type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                <button type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide-to="2" aria-label="Slide 3"></button>
                            </div>

                            <div className="carousel-inner">
                                <div className="carousel-item active">
                                    <img src="js-react-01.png" alt="Los Angeles" className="d-block w-100 rounded" />
                                </div>

                                <div className="carousel-item">
                                    <img src="js-react-02.png" alt="Chicago" className="d-block w-100 rounded" />
                                </div>

                                <div className="carousel-item">
                                    <img src="js-react-03.png" alt="New york" className="d-block w-100 rounded" />
                                </div>
                            </div>

                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                    <div className='coll coll-sm-2'>
                        <div className="card-body">
                            <h5 className="card-title">Demo - 2</h5>
                            <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="moreinfo-js-react" className="btn btn-primary">Run Demo</a>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default JsReactSolutions;
