import React from 'react'

export default function NotFound() {
    return (
        <div className="mx-5 p-5">
            <p className="text">
                Lost? <br />
                There is no place like <a href="/" className="text-reset">home</a>.               
            </p>
          
            <img src="lost.svg" alt="" className="img-fluid d-inline-block align-text-center" />
            </div>

    )
}
