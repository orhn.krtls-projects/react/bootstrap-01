import React from 'react'

function Solutions() {

    return (

        <div className="container-fluid mt-5 mb-5">
            <div className='row'>
                <div className='col-lg-4 col-md-6 col-xl-3'>
                    <div className='card" style="width: 18rem;'>
                        <div className="card-body">
                            <h5 className="card-title">Javascript,React</h5>
                            <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="js-react-solutions" className="btn btn-primary">More info</a>
                        </div>
                    </div>
                </div>
                <div className='col-lg-4 col-md-6 col-xl-3'>
                    <div className='card" style="width: 18rem;'>
                        <div className="card-body">
                            <h5 className="card-title">Go language</h5>
                            <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="golang-solutions" className="btn btn-primary">More info</a>
                        </div>
                    </div>
                </div>
                <div className='col-lg-4 col-md-6 col-xl-3'>
                    <div className='card" style="width: 18rem;'>
                        <div className="card-body">
                            <h5 className="card-title">Java</h5>
                            <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="java-springboot-solutions" className="btn btn-primary">More info</a>
                        </div>
                    </div>
                </div>
                <div className='col-lg-4 col-md-6 col-xl-3'>
                    <div className='card" style="width: 18rem;'>
                        <div className="card-body">
                            <h5 className="card-title">Redis NoSql</h5>
                            <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="redis-solutions" className="btn btn-primary">More info</a>
                        </div>
                    </div>
                </div>

                <div className='col-lg-4 col-md-6 col-xl-3'>
                    <div className='card" style="width: 18rem;'>
                        <div className="card-body">
                            <h5 className="card-title">ELK/EFK</h5>
                            <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="elk-ekf-solutions" className="btn btn-primary">More info</a>
                        </div>
                    </div>
                </div>
                <div className='col-lg-4 col-md-6 col-xl-3'>
                    <div className='card" style="width: 18rem;'>
                        <div className="card-body">
                            <h5 className="card-title">CI/CD</h5>
                            <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" className="btn btn-primary">More info</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Solutions;